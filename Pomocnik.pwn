//Miejsce na stopkę
 
//Include's
#include <a_samp>
#include <a_mysql>
#include <fixes>
#include <YSI\y_iterate>
#include <YSI\y_timers>
#include <kolory>
#include <zcmd>
#include <sscanf>
#include <dini2>
#include <kickfix>
#include <md5>
 
//Module's
#include "libs/definitions.inc"
#include "libs/enums.inc"
#include "libs/textdraws.inc"
#include "libs/dialogs.inc"
#include "libs/functions.inc"
#include "libs/load.inc"
#include "libs/player.inc"
#include "libs/others.inc"
#include "libs/noclip.inc"
#include "libs/player_cmd.inc"
#include "libs/timers.inc"
#include "libs/items.inc"
 
main() {}
 
//GM publics
public OnGameModeInit()
{
    LoadMySQL();
    LoadTextdraws();
    LoadClass();
    LoadConfig();
 
    SetTimer("settime",1000,true);
 
    return 1;
}
 
public OnGameModeExit()
{
    N_End();
 
    return 1;
}
 
//Player publics
public OnPlayerSpawn(playerid)
{
    SetPlayerSkin(playerid, 271);
    StopAudioStreamForPlayer(playerid);
    TextDrawHideForPlayer(playerid, WelcomeTextDraw);
    Attach3DTextLabelToPlayer(pPlayer[playerid][char_name_label], playerid, 0.0, 0.0, 0.1);
    UpdatePlayerStatus(playerid);
    return 1;
}
 
public OnPlayerDisconnect(playerid)
{
    Update3DTextLabelText(pPlayer[playerid][char_name_label], 0xFFFFFF99, "");
    return 1;
}
 
public OnPlayerDeath(playerid)
{
    return 1;
}
 
public OnPlayerConnect(playerid)
{
    N_Player(playerid);
 
    TextDrawShowForPlayer(playerid, RadioTextDraw);
    TextDrawShowForPlayer(playerid, SerwerTextDraw);
    TextDrawShowForPlayer(playerid, WelcomeTextDraw);
    TextDrawShowForPlayer(playerid, Time);
    TextDrawShowForPlayer(playerid, Date);
 
    AllowAdminTeleport(1);
    AllowPlayerTeleport(playerid, 1);
 
    if(IsPlayerAdmin(playerid)) SetPlayerColor(playerid, 0x990000FF);
    else SetPlayerColor(playerid, 0xC0C0C0FF);
 
    return 1;
}
 
public OnPlayerRequestClass(playerid, classid)
{
    mysql_query(sprintf("SELECT `char_uid` FROM `gamemode_players` WHERE `char_name` = '%s'", PlayerName(playerid)));
    mysql_store_result();
    if(mysql_num_rows())
    {
        ShowPlayerDialog(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "Logowanie", "Podaj haslo, aby sie zalogowac.", "Wybierz", "Anuluj");
    }
    else
    {
        SendClientMessage(playerid, 0xFFFFFFFF, " \nAby wejść na serwer, musisz najpierw zaakceptować {C0C0C0}regulamin{FFFFFF}!");
        ShowPlayerDialog(playerid, DIALOG_RULES, DIALOG_STYLE_MSGBOX, "Regulamin", "{FFFFFF}1. Uzytkownik moze zostac zbanowany bez podania powodu wylacznie przez glowna administracje.\n2. Prowokowanie innych osob do popelnienia czynu opisanego wyzej bedzie skutkowalo banicja na okres trzydziestu dni.\n3. Obrazy w kierunku innych uzytkownikow serwera beda surowo karane.\n4. Zabrania sie reklamowania innych serwisow.\n5. Wulgaryzmy sa dozwolone.\n\n{FF0000}Akceptujesz regulamin?", "Tak", "Nie");
    }
    TogglePlayerSpectating(playerid, 1);
   
    PlayAudioStreamForPlayer(playerid, "http://www.rmfon.pl/n/rmfmaxxx.pls");

    InterpolateCameraPos(playerid, 1800.654907, -1061.519897, 133.729736, 1752.653686, -1192.006835, 109.957901, 3000);
    InterpolateCameraLookAt(playerid, 1800.587890, -1066.482055, 133.119201, 1747.654418, -1191.947631, 109.899307, 3000);
    return 1;
}
 
public OnPlayerClickMap(playerid, Float:fX, Float:fY, Float:fZ)
{
    SetPlayerPosFindZ(playerid, fX, fY, fZ);
    return 1;
}
 
public OnPlayerText(playerid, text[])
{
    if(IsPlayerAdmin(playerid)) SendClientMessageToAll(0xFFFFFFFF, sprintf("{cc3b3d}%s{FFFFFF}: {cc3b3d}%s", PlayerName(playerid), text));
    else SendClientMessageToAll(0xFFFFFFFF, sprintf("{808080b}%s{FFFFFF}: {C0C0C0}%s", PlayerName(playerid), text));
 
    return 0;
}
 
public OnPlayerCommandPerformed(playerid, cmdtext[], success)
{
    if(!success) TextDrawShowForPlayer(playerid, CommandTextDraw);
    SetTimerEx("cmd", 2000, false, "i", playerid);
    PlayerPlaySound(playerid, 1190, 0, 0, 0);
    return 1;
}
 
public OnPlayerUpdate(playerid)
{
    N_PUpdate(playerid);
    return 1;
}
 
//Player (vehicle) publics
public OnPlayerEnterVehicle(playerid, vehicleid, ispassenger)
{
    SetPlayerChatBubble(playerid, "Wchodzi do pojazdu i odpala silnik", 0xFF0000FF, 100.0, 10000);
    return 1;
}
 
public OnPlayerExitVehicle(playerid, vehicleid)
{
    SetPlayerChatBubble(playerid, "Wychodzi z pojazdu i gasi silnik", 0xFF0000FF, 100.0, 10000);
    return 1;
}