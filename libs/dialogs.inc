public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
    if(dialogid == DIALOG_GUNS)
    {
        if(response == 1)
        {
            switch(listitem)
            {
                case 1:
                {
                    ShowPlayerDialog(playerid, DIALOG_PISTOLS, DIALOG_STYLE_TABLIST, "Bronie -> Pistolety", "# Pistolety #\t\nColt-45\t({C0C0C0}ID: 22{FFFFFF})\nSilenced 9mm\t({C0C0C0}ID: 23{FFFFFF})\nDesert Eagle\t({C0C0C0}ID: 24{FFFFFF})", "Wybierz", "Zamknij");
                }
                case 2:
                {
                    ShowPlayerDialog(playerid, DIALOG_RIFLES, DIALOG_STYLE_TABLIST, "Bronie -> Karabiny", "# Karabiny #\t\nAK-47\t({C0C0C0}ID: 30{FFFFFF})\nM4A1\t({C0C0C0}ID: 31{FFFFFF})", "Wybierz", "Zamknij");
                }
                case 3:
                {
                    ShowPlayerDialog(playerid, DIALOG_SHOTGUNS, DIALOG_STYLE_TABLIST, "Bronie -> Shotguny", "# Shotguny #\t\nShotgun\t({C0C0C0}ID: 25{FFFFFF})\nSawnoff Shotgun\t({C0C0C0}ID: 26{FFFFFF})\nCombat Shotgun\t({C0C0C0}ID: 27{FFFFFF})", "Wybierz", "Zamknij");
                }
                case 4:
                {
                    ShowPlayerDialog(playerid, DIALOG_SNIPERS, DIALOG_STYLE_TABLIST, "Bronie -> Snajperki", "# Snajperki #\t\nCountry Rifle\t({C0C0C0}ID: 33{FFFFFF})\nSniper Rifle\t({C0C0C0}ID: 34{FFFFFF})", "Wybierz", "Zamknij");
                }
                case 5:
                {
                    ShowPlayerDialog(playerid, DIALOG_MACHINE, DIALOG_STYLE_TABLIST, "Bronie -> Maszynowe", "# Maszynowe #\t\nUzi\t{C0C0C0}(ID: 28{FFFFFF})\nMP5\t({C0C0C0}ID: 29{FFFFFF})\nTec-9\t({C0C0C0}ID: 32{FFFFFF})", "Wybierz", "Zamknij");
                }
                case 7:
                {
                    ShowPlayerDialog(playerid, DIALOG_EROTICS, DIALOG_STYLE_TABLIST, "Bronie -> Erotyczne", "# Erotyczne #\t\nDildo\t({C0C0C0}ID: 11{FFFFFF})\nWibrator\t({C0C0C0}ID: 12{FFFFFF})", "Wybierz", "Zamknij");
                }
            } 
        }
    }
    else if(dialogid == DIALOG_PISTOLS)
    {
        if(response == 1)
        {
            switch(listitem)
            {
                case 1:
                {
                    GivePlayerWeapon(playerid, 22, 1000);
                }
                case 2:
                {
                    GivePlayerWeapon(playerid, 23, 1000);
                }
                case 3:
                {
                    GivePlayerWeapon(playerid, 24, 1000);
                }
            }
        }
    }
    else if(dialogid == DIALOG_RIFLES)
    {
        if(response == 1)
        {
            switch(listitem)
            {
                case 1:
                {
                    GivePlayerWeapon(playerid, 30, 1000);
                }
                case 2:
                {
                    GivePlayerWeapon(playerid, 31, 1000);
                }
            }
        }
    }
    else if(dialogid == DIALOG_SHOTGUNS)
    {
        if(response == 1)
        {
            switch(listitem)
            {
                case 1:
                {
                    GivePlayerWeapon(playerid, 25, 1000);
                }
                case 2:
                {
                   GivePlayerWeapon(playerid, 26, 1000); 
                }
                case 3:
                {
                    GivePlayerWeapon(playerid, 27, 1000);
                }
            }
        }
    }
    else if(dialogid == DIALOG_SNIPERS)
    {
        if(response == 1)
        {
            switch(listitem)
            {
                case 1:
                {
                    GivePlayerWeapon(playerid, 33, 1000);
                }
                case 2:
                {
                    GivePlayerWeapon(playerid, 34, 1000);
                }
            }
        }
    }
    else if(dialogid == DIALOG_MACHINE)
    {
        if(response == 1)
        {
            switch(listitem)
            {
                case 1:
                {
                    GivePlayerWeapon(playerid, 28, 1000);
                }
                case 2:
                {
                    GivePlayerWeapon(playerid, 29, 1000);
                }
                case 3:
                {
                    GivePlayerWeapon(playerid, 32, 1000);
                }
            }
        }
    }
    else if(dialogid == DIALOG_EROTICS)
    {
        if(response == 1)
        {
            switch(listitem)
            {
                case 1:
                {
                    GivePlayerWeapon(playerid, 10, 1);
                }
                case 2:
                {
                    GivePlayerWeapon(playerid, 12, 1);
                }
            }
        }
    }
    else if(dialogid == DIALOG_RULES)
    {
        if(response == 1)
        {
            SetSpawnInfo(playerid, 0, 0, 1724.2443,-1949.3219,14.1172,89.0624, 0, 0, 0, 0, 0, 0);
            SpawnPlayer(playerid);
            TogglePlayerSpectating(playerid, 0);
            new string[128], name[MAX_PLAYER_NAME];
            GetPlayerName(playerid,name,MAX_PLAYER_NAME);
            format(string,sizeof string,"{FFFFFF}Gracz {C0C0C0}%s (ID: %d) {FFFFFF}wlasnie polaczyl sie z {C0C0C0}serwer-pomocniczy.pl{FFFFFF}!", name, playerid);
            SendClientMessageToAll(-1,string);
            TextDrawShowForPlayer(playerid, SkinTextDraw);
            SetTimerEx("skin", 5000, false, "i", playerid);
            return 1;
        }
        if(response == 0)
        {
            Kick(playerid);
            return 1;
        }
    }
    else if(dialogid == DIALOG_HELP)
    {
        if(response == 1)
        {
            switch(listitem)
            {
                case 0:
                {
                    ShowPlayerDialog(playerid, DIALOG_OPTIONS, DIALOG_STYLE_TABLIST, "Pomoc -> Opcje Osobiste", "Nick:\t\nTryb FPS:\tOff\nIlosc warnow:\t0/3", "OK", "");
                }
                case 1:
                {
                    ShowPlayerDialog(playerid, DIALOG_COMMANDS, DIALOG_STYLE_TABLIST, "Pomoc -> Komendy", "/stats\t<- Wyswietla Twoje aktualne statystyki\n/fly\t<- Wlacza/wylacza latanie\n/v\t<- Informacje o pojazdach\n/world\t<- Zmienia wirtualny swiat Twojej postaci\n/bronie\t<- Zbior wszystkich broni\n/hp\t<- Dodajesz sobie HP postaci\n/a\t<- Wyswietla czlonkow ekipy\n/skin\t<- Zmieniasz skina swojej postaci\n/radio\t<- Wlaczasz/wylaczasz radio\n/login\t<- Logujesz sie na nowo", "OK", "");
                }
                case 2:
                {
                    ShowPlayerDialog(playerid, DIALOG_ANIMATIONS, DIALOG_STYLE_TABLIST, "Pomoc -> Animacje", "Brak", "OK", "");
                }
                case 3:
                {
                    ShowPlayerDialog(playerid, DIALOG_FAQ, DIALOG_STYLE_TABLIST, "Pomoc -> FAQ - Pytania & Odpowiedzi", "Brak", "OK", "");
                }
            }
        }
    }
    else if(dialogid == DIALOG_MENU_RADIO)
    {
        if(response == 1)
        {
            switch(listitem)
            {
                case 1:
                {
                    PlayAudioStreamForPlayer(playerid, "http://www.rmfon.pl/n/rmffm.pls");
                    SendClientMessage(playerid, 0xFFFFFFFF, "Wybrales radiostacje: RMF FM!");
                    SendClientMessage(playerid, 0xFFFFFFFF, "Aby wylaczyc radio, wpisz {C0C0C0}/wylaczradio");
                    TextDrawShowForPlayer(playerid, radioon);
                    SetTimerEx("radioopen", 2000, false, "i", playerid);
                }
                case 2:
                {
                    PlayAudioStreamForPlayer(playerid, "http://www.rmfon.pl/n/rmfmaxxx.pls");
                    SendClientMessage(playerid, 0xFFFFFFFF, "Wybrales radiostacje: RMF MAXXX!");
                    SendClientMessage(playerid, 0xFFFFFFFF, "Aby wylaczyc radio, wpisz {C0C0C0}/wylaczradio");
                    TextDrawShowForPlayer(playerid, radioon);
                    SetTimerEx("radioopen", 2000, false, "i", playerid);
                }
            }
        }
    }
    else if(dialogid == DIALOG_REGISTER)
    {
        if(!response) return 1;
        mysql_query(sprintf("INSERT INTO `gamemode_players` (`char_uid`, `char_name`, `char_pass`, `char_cash`) VALUES ('NULL', '%s', '%s', '1000')", PlayerName(playerid), MD5_Hash(inputtext)));
        SendClientMessage(playerid, -1, sprintf("Pomyslnie zarejestrowales swoje konto! [NICK: %s, UID: %d]", PlayerName(playerid), mysql_insert_id()));
    }
    else if(dialogid == DIALOG_LOGIN)
    {
        if(!response) return Kick(playerid);
        new data[128], pass[128];
        mysql_query(sprintf("SELECT char_pass FROM gamemode_players WHERE char_name = '%s'", PlayerName(playerid)));
        mysql_store_result();
        while(mysql_fetch_row_format(data, "|"))
        {
            sscanf(data,"p<|>s[128]", pass);
        }
        if(!strcmp(MD5_Hash(inputtext), pass))
        {
            SetSpawnInfo(playerid, 0, 0, 1724.2443,-1949.3219,14.1172,89.0624, 0, 0, 0, 0, 0, 0);
            SpawnPlayer(playerid);
            TogglePlayerSpectating(playerid, 0);
            new string[128], name[MAX_PLAYER_NAME];
            GetPlayerName(playerid,name,MAX_PLAYER_NAME);
            format(string,sizeof string,"{FFFFFF}Gracz {C0C0C0}%s (ID: %d) {FFFFFF}wlasnie polaczyl sie z {C0C0C0}serwer-pomocniczy.pl{FFFFFF}!", name, playerid);
            SendClientMessageToAll(-1,string);
            TextDrawShowForPlayer(playerid, SkinTextDraw);
            SetTimerEx("skin", 5000, false, "i", playerid);
            OnPlayerLogin(playerid);
        }
        else
        {
            ShowPlayerDialog(playerid, DIALOG_LOGIN, DIALOG_STYLE_PASSWORD, "Logowanie > bledne haslo!", "Wprowadz haslo by sie zalogowac!\n\nPodano bledne haslo!", "Wybierz", "Wyjdz");
        }
        return 1;
    }
    return 1;
}