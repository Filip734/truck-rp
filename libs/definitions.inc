//Config's
#define CONFIG_FILEPATCH "server/config.ini"

//Macro's
new sprintfStr[500];
#define sprintf(%0,%1) (format(sprintfStr, 1000, %0, %1), sprintfStr)

#define foreachEx(%2,%1) for(new %2 = 0; %2 < %1; %2++)

//Global variables

new Float:pX,
	Float:pY,
	Float:pZ,
	Float:pAngle;

new VehicleNames[212][] = {
{"Landstalker"},{"Bravura"},{"Buffalo"},{"Linerunner"},{"Perrenial"},{"Sentinel"},{"Dumper"},
{"Firetruck"},{"Trashmaster"},{"Stretch"},{"Manana"},{"Infernus"},{"Voodoo"},{"Pony"},{"Mule"},
{"Cheetah"},{"Ambulance"},{"Leviathan"},{"Moonbeam"},{"Esperanto"},{"Taxi"},{"Washington"},
{"Bobcat"},{"Mr Whoopee"},{"BF Injection"},{"Hunter"},{"Premier"},{"Enforcer"},{"Securicar"},
{"Banshee"},{"Predator"},{"Bus"},{"Rhino"},{"Barracks"},{"Hotknife"},{"Trailer 1"},{"Previon"},
{"Coach"},{"Cabbie"},{"Stallion"},{"Rumpo"},{"RC Bandit"},{"Romero"},{"Packer"},{"Monster"},
{"Admiral"},{"Squalo"},{"Seasparrow"},{"Pizzaboy"},{"Tram"},{"Trailer 2"},{"Turismo"},
{"Speeder"},{"Reefer"},{"Tropic"},{"Flatbed"},{"Yankee"},{"Caddy"},{"Solair"},{"Berkley's RC Van"},
{"Skimmer"},{"PCJ-600"},{"Faggio"},{"Freeway"},{"RC Baron"},{"RC Raider"},{"Glendale"},{"Oceanic"},
{"Sanchez"},{"Sparrow"},{"Patriot"},{"Quad"},{"Coastguard"},{"Dinghy"},{"Hermes"},{"Sabre"},
{"Rustler"},{"ZR-350"},{"Walton"},{"Regina"},{"Comet"},{"BMX"},{"Burrito"},{"Camper"},{"Marquis"},
{"Baggage"},{"Dozer"},{"Maverick"},{"News Chopper"},{"Rancher"},{"FBI Rancher"},{"Virgo"},{"Greenwood"},
{"Jetmax"},{"Hotring"},{"Sandking"},{"Blista Compact"},{"Police Maverick"},{"Boxville"},{"Benson"},
{"Mesa"},{"RC Goblin"},{"Hotring Racer A"},{"Hotring Racer B"},{"Bloodring Banger"},{"Rancher"},
{"Super GT"},{"Elegant"},{"Journey"},{"Bike"},{"Mountain Bike"},{"Beagle"},{"Cropdust"},{"Stunt"},
{"Tanker"}, {"Roadtrain"},{"Nebula"},{"Majestic"},{"Buccaneer"},{"Shamal"},{"Hydra"},{"FCR-900"},
{"NRG-500"},{"HPV1000"},{"Cement Truck"},{"Tow Truck"},{"Fortune"},{"Cadrona"},{"FBI Truck"},
{"Willard"},{"Forklift"},{"Tractor"},{"Combine"},{"Feltzer"},{"Remington"},{"Slamvan"},
{"Blade"},{"Freight"},{"Streak"},{"Vortex"},{"Vincent"},{"Bullet"},{"Clover"},{"Sadler"},
{"Firetruck LA"},{"Hustler"},{"Intruder"},{"Primo"},{"Cargobob"},{"Tampa"},{"Sunrise"},{"Merit"},
{"Utility"},{"Nevada"},{"Yosemite"},{"Windsor"},{"Monster A"},{"Monster B"},{"Uranus"},{"Jester"},
{"Sultan"},{"Stratum"},{"Elegy"},{"Raindance"},{"RC Tiger"},{"Flash"},{"Tahoma"},{"Savanna"},
{"Bandito"},{"Freight Flat"},{"Streak Carriage"},{"Kart"},{"Mower"},{"Duneride"},{"Sweeper"},
{"Broadway"},{"Tornado"},{"AT-400"},{"DFT-30"},{"Huntley"},{"Stafford"},{"BF-400"},{"Newsvan"},
{"Tug"},{"Trailer 3"},{"Emperor"},{"Wayfarer"},{"Euros"},{"Hotdog"},{"Club"},{"Freight Carriage"},
{"Trailer 3"},{"Andromada"},{"Dodo"},{"RC Cam"},{"Launch"},{"Police Car (LSPD)"},{"Police Car (SFPD)"},
{"Police Car (LVPD)"},{"Police Ranger"},{"Picador"},{"S.W.A.T. Van"},{"Alpha"},{"Phoenix"},{"Glendale"},
{"Sadler"},{"Luggage Trailer A"},{"Luggage Trailer B"},{"Stair Trailer"},{"Boxville"},{"Farm Plow"},
{"Utility Trailer"}};

//Other's
#define MOVE_SPEED              100.0
#define ACCEL_RATE              0.03

#define CAMERA_MODE_NONE        100
#define CAMERA_MODE_FLY         101

#define MOVE_FORWARD            200
#define MOVE_BACK               201
#define MOVE_LEFT               202
#define MOVE_RIGHT              203
#define MOVE_FORWARD_LEFT       204
#define MOVE_FORWARD_RIGHT      205
#define MOVE_BACK_LEFT          206
#define MOVE_BACK_RIGHT         207

//Dialog's
#define DIALOG_NONE				9999
#define DIALOG_RULES 			0
#define DIALOG_GUNS  			1
#define DIALOG_PISTOLS 			2
#define DIALOG_SHOTGUNS  		3
#define DIALOG_SNIPERS  		4
#define DIALOG_MACHINE			5
#define DIALOG_RIFLES	 		6
#define DIALOG_EROTICS 			7
#define DIALOG_MENU_RADIO 		8
#define DIALOG_HELP 			9
#define DIALOG_OPTIONS 			10
#define DIALOG_COMMANDS 		11
#define DIALOG_ANIMATIONS 		12
#define DIALOG_FAQ 				13
#define DIALOG_REGISTER			14
#define DIALOG_LOGIN			15

//Default color's
#define COLOR_GREY 				0x808080FF
#define COLOR_YELLOW            0xFFFF00FF

//Hex color's
#define HEX_COLOR_WHITE			"{FFFFFF}"
#define HEX_COLOR_YELLOW		"{FAF623}"
#define HEX_COLOR_ORANGE 		"{F2C80C}"
#define HEX_COLOR_RED			"{FF002B}"
#define HEX_COLOR_GREEN			"{3DE3B1}"
#define HEX_COLOR_GREY			"{C0C0C0}"

//Owner type's
#define OWNER_NONE		0
#define OWNER_PLAYER	1
#define OWNER_VEHICLE	2
#define OWNER_GROUND	3

//Iterator's
#define MAX_ITEMS	10000
new Iterator:Items<MAX_ITEMS>;