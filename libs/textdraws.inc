new Text:SerwerTextDraw;
new Text:RadioTextDraw;
new Text:WelcomeTextDraw;
new Text:SkinTextDraw;
new Text:radioon;
new Text:radiooff;
new Text:Date;
new Text:Time;
new Text:RestartTextDraw;
new Text:CommandTextDraw;
new Text:FlyON;
new Text:FlyOFF;
new Text:VehicleSpawn;

stock LoadTextdraws()
{
    RadioTextDraw = TextDrawCreate(1.797235, 437.528747, "~bhh~~h~Radio Los Santos~w~~h~ ~>~ Odtwarzany jest standardowy blok muzyczny.. ((/radio))");
    TextDrawLetterSize(RadioTextDraw,0.190987, 0.993281);
    TextDrawTextSize(RadioTextDraw,1236.000000, 0.000000);
    TextDrawAlignment(RadioTextDraw, 1);
    TextDrawColor(RadioTextDraw, -1);
    TextDrawUseBox(RadioTextDraw, 1);
    TextDrawBoxColor(RadioTextDraw,  15);
    TextDrawSetShadow(RadioTextDraw, 0);
    TextDrawSetOutline(RadioTextDraw, 1);
    TextDrawBackgroundColor(RadioTextDraw, 83);
    TextDrawFont(RadioTextDraw, 1);
    TextDrawSetProportional(RadioTextDraw, 1);
    TextDrawSetShadow(RadioTextDraw, 0);

    SerwerTextDraw = TextDrawCreate(89,424,"~h~serwer~bhh~~h~-~w~~h~pomocniczy~bhh~~h~.~w~~h~pl");
    TextDrawLetterSize(SerwerTextDraw,0.249999,1.100000);
    TextDrawAlignment(SerwerTextDraw,3);
    TextDrawBackgroundColor(SerwerTextDraw,0x000000ff);
    TextDrawFont(SerwerTextDraw,1);
    TextDrawUseBox(SerwerTextDraw,0);
    TextDrawSetOutline(SerwerTextDraw,0);
    TextDrawSetProportional(SerwerTextDraw,1);
    TextDrawSetShadow(SerwerTextDraw,1);

    WelcomeTextDraw = TextDrawCreate(207.000000, 65.000000, "~h~serwer~bhh~~h~-~w~~h~pomocniczy~bhh~~h~.~w~~h~pl");
    TextDrawFont(WelcomeTextDraw, 1);
    TextDrawLetterSize(WelcomeTextDraw, 0.679165, 2.499999);
    TextDrawTextSize(WelcomeTextDraw, 490.500000, 17.000000);
    TextDrawSetOutline(WelcomeTextDraw, 1);
    TextDrawSetShadow(WelcomeTextDraw, 0);
    TextDrawAlignment(WelcomeTextDraw, 1);
    TextDrawColor(WelcomeTextDraw, -1);
    TextDrawBackgroundColor(WelcomeTextDraw, 255);
    TextDrawBoxColor(WelcomeTextDraw, 50);
    TextDrawUseBox(WelcomeTextDraw, 0);
    TextDrawSetProportional(WelcomeTextDraw, 1);
    TextDrawSetSelectable(WelcomeTextDraw, 0);

    SkinTextDraw = TextDrawCreate(500,103,"~h~Aby wybrac innego skina uzyj komendy ~bhh~/skin (ID)~w~~h~.~n~~>~Zyczymy milej i udanej gry!~<~~n~   ~>~serwer-pomocniczy.pl~<~");
    TextDrawLetterSize(SkinTextDraw,0.199999,1.100000);
    TextDrawAlignment(SkinTextDraw,0);
    TextDrawBackgroundColor(SkinTextDraw,0x000000ff);
    TextDrawFont(SkinTextDraw,1);
    TextDrawUseBox(SkinTextDraw,1);
    TextDrawBoxColor(SkinTextDraw,0x00000099);
    TextDrawTextSize(SkinTextDraw,605.000000,36.000000);
    TextDrawSetOutline(SkinTextDraw,0);
    TextDrawSetProportional(SkinTextDraw,1);
    TextDrawSetShadow(SkinTextDraw,0);

    radioon = TextDrawCreate(207.000000, 65.000000, "~p~RADIOSTACJA: ~w~ON");
    TextDrawFont(radioon, 1);
    TextDrawLetterSize(radioon, 0.679165, 2.499999);
    TextDrawTextSize(radioon, 490.500000, 17.000000);
    TextDrawSetOutline(radioon, 1);
    TextDrawSetShadow(radioon, 0);
    TextDrawAlignment(radioon, 1);
    TextDrawColor(radioon, -1);
    TextDrawBackgroundColor(radioon, 255);
    TextDrawBoxColor(radioon, 50);
    TextDrawUseBox(radioon, 0);
    TextDrawSetProportional(radioon, 1);
    TextDrawSetSelectable(radioon, 0);

    radiooff = TextDrawCreate(207.000000, 65.000000, "~p~RADIOSTACJA: ~w~OFF");
    TextDrawFont(radiooff, 1);
    TextDrawLetterSize(radiooff, 0.679166, 2.500000);
    TextDrawTextSize(radiooff, 450.000000, 37.000000);
    TextDrawSetOutline(radiooff, 1);
    TextDrawSetShadow(radiooff, 0);
    TextDrawAlignment(radiooff, 1);
    TextDrawColor(radiooff, -1);
    TextDrawBackgroundColor(radiooff, 255);
    TextDrawBoxColor(radiooff, 50);
    TextDrawUseBox(radiooff, 0);
    TextDrawSetProportional(radiooff, 1);
    TextDrawSetSelectable(radiooff, 0);


    Date = TextDrawCreate(546,32,"--");
    TextDrawLetterSize(Date,0.349999,1.100000);
    TextDrawAlignment(Date,0);
    TextDrawBackgroundColor(Date,0x000000ff);
    TextDrawFont(Date,1);
    TextDrawUseBox(Date,0);
    TextDrawSetOutline(Date,0);
    TextDrawSetProportional(Date,1);
    TextDrawSetShadow(Date,1);
    
    Time = TextDrawCreate(548,43,"--");
    TextDrawLetterSize(Time,0.399999,1.100000);
    TextDrawAlignment(Time,0);
    TextDrawBackgroundColor(Time,0x000000ff);
    TextDrawFont(Time,1);
    TextDrawUseBox(Time,0);
    TextDrawSetOutline(Time,0);
    TextDrawSetProportional(Time,1);
    TextDrawSetShadow(Time,1);

    RestartTextDraw = TextDrawCreate(319,193,"~h~RESTART SERWERA~n~ZAPRASZAMY ZA MOMENT!");
    TextDrawLetterSize(RestartTextDraw,0.399999,2.299999);
    TextDrawAlignment(RestartTextDraw,2);
    TextDrawBackgroundColor(RestartTextDraw,0x000000ff);
    TextDrawFont(RestartTextDraw,1);
    TextDrawUseBox(RestartTextDraw,1);
    TextDrawBoxColor(RestartTextDraw,0xE1000066);
    TextDrawTextSize(RestartTextDraw,419.000000,203.000000);
    TextDrawSetOutline(RestartTextDraw,0);
    TextDrawSetProportional(RestartTextDraw,1);
    TextDrawSetShadow(RestartTextDraw,1);

    CommandTextDraw = TextDrawCreate(502,104,"~h~Niepoprawna komenda badz nie posiadasz do niej uprawnien!");
    TextDrawLetterSize(CommandTextDraw,0.249999,1.000000);
    TextDrawAlignment(CommandTextDraw,0);
    TextDrawBackgroundColor(CommandTextDraw,0x000000ff);
    TextDrawFont(CommandTextDraw,1);
    TextDrawUseBox(CommandTextDraw,1);
    TextDrawBoxColor(CommandTextDraw,0x00000099);
    TextDrawTextSize(CommandTextDraw,602.000000,114.000000);
    TextDrawSetOutline(CommandTextDraw,0);
    TextDrawSetProportional(CommandTextDraw,1);
    TextDrawSetShadow(CommandTextDraw,1);

    FlyON = TextDrawCreate(502,103,"~h~Latanie ~r~wlaczone~w~!          Uzyj ponownie komendy ~r~/fly ~w~~h~aby wylaczyc.");
    TextDrawLetterSize(FlyON,0.199999,1.100000);
    TextDrawAlignment(FlyON,0);
    TextDrawBackgroundColor(FlyON,0x000000ff);
    TextDrawFont(FlyON,1);
    TextDrawUseBox(FlyON,1);
    TextDrawBoxColor(FlyON,0x00000066);
    TextDrawTextSize(FlyON,602.000000,113.000000);
    TextDrawSetOutline(FlyON,0);
    TextDrawSetProportional(FlyON,1);
    TextDrawSetShadow(FlyON,1);

    FlyOFF = TextDrawCreate(502,103,"~h~Latanie ~r~wylaczone~w~!         Uzyj ponownie komendy ~r~/fly ~w~~h~aby wylaczyc.");
    TextDrawLetterSize(FlyOFF,0.199999,1.100000);
    TextDrawAlignment(FlyOFF,0);
    TextDrawBackgroundColor(FlyOFF,0x000000ff);
    TextDrawFont(FlyOFF,1);
    TextDrawUseBox(FlyOFF,1);
    TextDrawBoxColor(FlyOFF,0x00000066);
    TextDrawTextSize(FlyOFF,602.000000,113.000000);
    TextDrawSetOutline(FlyOFF,0);
    TextDrawSetProportional(FlyOFF,1);
    TextDrawSetShadow(FlyOFF,1);

    VehicleSpawn = TextDrawCreate(502,103,"~w~Pojazd zostal pomyslnie zespawnowany!");
    TextDrawLetterSize(VehicleSpawn,0.199999,1.100000);
    TextDrawAlignment(VehicleSpawn,0);
    TextDrawBackgroundColor(VehicleSpawn,0x000000ff);
    TextDrawFont(VehicleSpawn,1);
    TextDrawUseBox(VehicleSpawn,1);
    TextDrawBoxColor(VehicleSpawn,0x00000066);
    TextDrawTextSize(VehicleSpawn,602.000000,113.000000);
    TextDrawSetOutline(VehicleSpawn,0);
    TextDrawSetProportional(VehicleSpawn,1);
    TextDrawSetShadow(VehicleSpawn,1);
    return 1;
}