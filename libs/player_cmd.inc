CMD:debug(playerid, params[])
{
    UpdatePlayerStatus(playerid);
    return 1;
}

CMD:v(playerid, params[])
{
        new Vehicle[32], VehicleID, ColorOne, ColorTwo;
        pPlayer[playerid][pCanSpawnVehicle] = true;
        if(sscanf(params, "s[32]D(1)D(1)", Vehicle, ColorOne, ColorTwo))
        {
            pPlayer[playerid][pCanSpawnVehicle] = true;
            SendClientMessage(playerid, COLOR_GREY, "Poprawne uzycie: {C0C0C0}/v [Nazwa pojazdu/ID pojazdu] [Kolor 1 (opcjonalny)] [Kolor 2 (opcjonalny)]");
            return 1;
        }
       
        if(pPlayer[playerid][pCanSpawnVehicle])
        {
            VehicleID = GetVehicleModelIDFromName(Vehicle);
            if(VehicleID != 425 && VehicleID != 432 && VehicleID != 447 &&
               VehicleID != 430 && VehicleID != 417 && VehicleID != 435 &&
           VehicleID != 446 && VehicleID != 449 && VehicleID != 450 &&
               VehicleID != 452 && VehicleID != 453 && VehicleID != 454 &&
                   VehicleID != 460 && VehicleID != 464 && VehicleID != 465 &&
                   VehicleID != 469 && VehicleID != 472 && VehicleID != 473 &&
                   VehicleID != 476 && VehicleID != 484 && VehicleID != 487 &&
                   VehicleID != 488 && VehicleID != 493 && VehicleID != 497 &&
                   VehicleID != 501 && VehicleID != 511 && VehicleID != 512 &&
                   VehicleID != 513 && VehicleID != 519 && VehicleID != 520 &&
                   VehicleID != 537 && VehicleID != 538 && VehicleID != 548 &&
                   VehicleID != 553 && VehicleID != 563 && VehicleID != 564 &&
                   VehicleID != 569 && VehicleID != 570 && VehicleID != 577 &&
                   VehicleID != 584 && VehicleID != 590 && VehicleID != 591 &&
                   VehicleID != 592 && VehicleID != 593 && VehicleID != 594 &&
                   VehicleID != 595 && VehicleID != 606 && VehicleID != 607 &&
                   VehicleID != 608 && VehicleID != 610 && VehicleID != 611) {
                        if(VehicleID == -1 )
                        {
                                VehicleID = strval(Vehicle);
 
                                if(VehicleID < 400 || VehicleID > 611 )
                                {
                                        return SendClientMessage(playerid, COLOR_GREY, "Wpisales niepoprawna nazwe pojazdu!");
                                }
                        }
 
                        GetPlayerPos(playerid, pX, pY, pZ);
                        GetPlayerFacingAngle(playerid, pAngle);
 
                        DestroyVehicle(pPlayer[playerid][pSpawnVehicle]);
                        pPlayer[playerid][pSpawnVehicle] = CreateVehicle(VehicleID, pX, pY, pZ+2.0, pAngle, ColorOne, ColorTwo, -1);
                        LinkVehicleToInterior(pPlayer[playerid][pSpawnVehicle], GetPlayerInterior(playerid));
                        PutPlayerInVehicle(playerid, pPlayer[playerid][pSpawnVehicle], 0);
                        TextDrawShowForPlayer(playerid, VehicleSpawn);
                        SetTimerEx("vehicle", 2000, false, "i", playerid);
                } else {
                    SendClientMessage(playerid, COLOR_GREY, "Nie masz uprawnien do spawnowania tego pojazdu!");
                }
        } else {
                SendClientMessage(playerid, COLOR_GREY, "Nie mozesz zespawnowac pojazdu w tym miejscu!");
        }
        return 1;
}

CMD:bronie(playerid, params[])
{
    ShowPlayerDialog(playerid, DIALOG_GUNS, DIALOG_STYLE_TABLIST, "Serwer Pomocniczy -> Bronie", "# Bronie #\t\nPistolety\t({C0C0C0}ILOSC: 3{FFFFFF})\nKarabiny\t({C0C0C0}ILOSC: 2{FFFFFF})\nShotguny\t({C0C0C0}ILOSC: 3{FFFFFF})\nSnajperki\t({C0C0C0}ILOSC: 2{FFFFFF})\nMaszynowe\t({C0C0C0}ILOSC: 3{FFFFFF})\n\t\nPrzedmioty erotyczne\t({C0C0C0}ILOSC: 2{FFFFFF})", "Potwierdz", "Anuluj");
    return 1;
}

CMD:pomoc(playerid, cmdtext[])
{
    ShowPlayerDialog(playerid, DIALOG_HELP, DIALOG_STYLE_LIST, "Serwer Pomocniczy --> Panel Pomocy", "#     Opcje osobiste\n#     Komendy\n#     Animacje\n#     FAQ - Pytania & Odpowiedzi", "Wybierz", "Wyjdz");
    return 1;
}

CMD:hp(playerid, params[])
{
        new give_playerid,
        Float:hp;
 
        if(sscanf(params, "df", give_playerid, hp))
        {
            SendClientMessage(playerid, 0x808080FF, "Uzycie komendy: {C0C0C0}/hp [ID gracza] [ilosc HP]");
            return 1;
        }
 
        if(!IsPlayerConnected(give_playerid))
        {
            SendClientMessage(playerid, 0xFFFFFFFF, "Gracz o podanym ID nie istnieje!");
            return 1;
        }
 
        if(hp < 0 || hp > 100)
        {
            SendClientMessage(playerid, 0xFFFFFFFF, "HP musi znajdowac sie pomiedzy wartoscia {C0C0C0}0 - 100{FFFFFF}.");
            return 1;
        }
 
        SetPlayerHealth(give_playerid, hp);
        SendClientMessage(give_playerid, 0xFFFFFFFF, "Otrzymales zdrowie!");
 
        SendClientMessage(playerid, 0xFFFFFFFF, "HP zostalo poprawnie nadane!");
       
        return 1;
 
}

CMD:login(playerid, params[])
{
    TogglePlayerSpectating(playerid, 1);
    ShowPlayerDialog(playerid, DIALOG_RULES, DIALOG_STYLE_MSGBOX, "Regulamin", "{FFFFFF}1. Uzytkownik moze zostac zbanowany bez podania powodu wylacznie przez glowna administracje.\n2. Prowokowanie innych osob do popelnienia czynu opisanego wyzej bedzie skutkowalo banicja na okres trzydziestu dni.\n3. Obrazy w kierunku innych uzytkownikow serwera beda surowo karane.\n4. Zabrania sie reklamowania innych serwisow.\n5. Wulgaryzmy sa dozwolone.\n\n{FF0000}Akceptujesz regulamin?", "Tak", "Nie");
    PlayAudioStreamForPlayer(playerid, "http://www.rmfon.pl/n/rmfmaxxx.pls");
    InterpolateCameraPos(playerid, 1800.654907, -1061.519897, 133.729736, 1752.653686, -1192.006835, 109.957901, 3000);
    InterpolateCameraLookAt(playerid, 1800.587890, -1066.482055, 133.119201, 1747.654418, -1191.947631, 109.899307, 3000);
    TextDrawShowForPlayer(playerid, WelcomeTextDraw);
    return 1;
}

CMD:naj(playerid, params[])
{
    new string[128], name[MAX_PLAYER_NAME];
    GetPlayerName(playerid,name,MAX_PLAYER_NAME);
    format(string,sizeof string,"Gracz {C0C0C0}%s [ID:%s] {FFFFFF}uwaza ze {C0C0C0}serwer-pomocniczy.pl {FFFFFF}jest najlepszy!", name, playerid);
    SendClientMessageToAll(0xFFFFFFFF,string);
    return 1;
}

CMD:skin(playerid, params[])
{
    new
        targetid,
        skinid;

    if(sscanf(params, "ud", targetid, skinid))
    {
        SendClientMessage(playerid, COLOR_GREY, "Poprawne uzycie: {C0C0C0}/skin [ID gracza] [ID skina]");
    }
    if(skinid > 316 || skinid < 0) 
    {
        SendClientMessage(playerid, 0xFFFFFFFF, "Dozwolone skiny: {C0C0C0}0 - 316{FFFFFF}.");
    }
    SetPlayerSkin(targetid, skinid);

    new adminname[MAX_PLAYER_NAME+1];
    GetPlayerName(playerid, adminname, sizeof(adminname));

    SendClientMessage(targetid, 0xFFFFFFFF, sprintf("{FF0000}Administrator {C0C0C0}%s {FFFFFF}zmienil Ci skina na {C0C0C0}%d{FFFFFF}.", adminname, skinid));

    new targetname[MAX_PLAYER_NAME+1];
    GetPlayerName(playerid, targetname, sizeof(targetname));

    SendClientMessage(playerid, 0xFFFFFFFF, sprintf("{FFFFFF}Zmieniles graczowi {C0C0C0}%s {FFFFFF}skina na {C0C0C0}%d{FFFFFF}.", targetname, skinid));

    return 1;
}

CMD:world(playerid, params[])
{
    new
        worldid;

    if(sscanf(params, "df", playerid, worldid))
    {
        SendClientMessage(playerid, COLOR_GREY, "Poprawne uzycie: {C0C0C0}/world [ID worldu]");
    }
    if(worldid > 1000 || worldid < 0) 
    {
        SendClientMessage(worldid, 0xFFFFFFFF, "Dozwolone worldy: {C0C0C0}0 - 1000{FFFFFF}.");
    }
    SetPlayerVirtualWorld(playerid, worldid);

    return 1;
}

CMD:radio(playerid, params[])
{
    ShowPlayerDialog(playerid, DIALOG_MENU_RADIO, DIALOG_STYLE_TABLIST, "Serwer Pomocniczy -> Radiostacje", "# Dostepne Radiostacje #\t\nRMF FM\t({009000}ONLINE{FFFFFF})\nRMF MAXXX\t({009000}ONLINE{FFFFFF})", "Wybierz", "Wyjdz");
    return 1;
}

CMD:wylaczradio(playerid, params[])
{
    StopAudioStreamForPlayer(playerid);
    TextDrawShowForPlayer(playerid, radiooff);
    SetTimerEx("radioclose", 2000, false, "i", playerid);
    return 1;
}

CMD:gmx(playerid, params[])
{
    if(IsPlayerAdmin(playerid))
    {
        TextDrawShowForAll(RestartTextDraw);
        SetTimerEx("restart", 2000, false, "i", playerid);
    }
    else
    {
        TextDrawShowForPlayer(playerid, CommandTextDraw);
        SetTimerEx("cmd", 2000, false, "i", playerid);
    }
    return 1;
}

CMD:slap(playerid, params[])
{
    if(IsPlayerAdmin(playerid))
    {
    new id, string[126], Float: PPos[3];
    if(sscanf(params, "u", id))
        return SendClientMessage(playerid, COLOR_GREY, "Poprawne uzycie: {C0C0C0}/slap [ID]");
    
    GetPlayerPos(id, PPos[0], PPos[1], PPos[2]);
    SetPlayerPos(id, PPos[0], PPos[1], PPos[2]+4);

    format(string, sizeof(string), "Poprawnie uderzyles gracza!");
    SendClientMessage(playerid, -1, string);
    }
    else
    {
        TextDrawShowForPlayer(playerid, CommandTextDraw);
        SetTimerEx("cmd", 2000, false, "i", playerid);
    }
    return 1;
}

CMD:a(playerid, params[])
{
    ShowPlayerDialog(playerid, DIALOG_NONE, DIALOG_STYLE_TABLIST, "Serwer Pomocniczy -> Ekipa:", "{FF0000}Administratorzy:\t\n{FFFFFF}Dxmin\t{FFFFFF}({009000}Online{FFFFFF}/{FF0000}Offline{FFFFFF})", "OK", "");
    return 1;
}

CMD:fly(playerid, params[])
{
    if(GetPVarType(playerid, "FlyMode"))
    {
        CancelFlyMode(playerid);
        TextDrawShowForPlayer(playerid, FlyOFF);
        SetTimerEx("lataniewylacz", 2000, false, "i", playerid);
    }
    else
    {
    FlyMode(playerid);
    TextDrawShowForPlayer(playerid, FlyOFF);
    SetTimerEx("lataniewlacz", 2000, false, "i", playerid);
    }
    return 1;
}

CMD:ap(playerid, params[])
{
    new uid = CreateItem(playerid, "Nazwa", 0, 0);
    SendClientMessage(playerid, -1, sprintf("Pomyslnie stworzono przedmiot o UID: %d", uid));
    return 1;
}

CMD:register(playerid, params[])
{
    mysql_query(sprintf("SELECT char_uid FROM gamemode_players WHERE char_name = '%s'", PlayerName(playerid)));
    mysql_store_result();
    if(mysql_num_rows()) return SendClientMessage(playerid, -1, "Takie konto juz istnieje!");
    ShowPlayerDialog(playerid, DIALOG_REGISTER, DIALOG_STYLE_INPUT, "Rejestracja", "Wprowadz haslo", "Wybierz", "Wyjdz");
    return 1;
}