stock LoadConfiguration()
{
    if( !dini_Exists(CONFIG_FILEPATH))
    {
        dini_Create(CONFIG_FILEPATH);
        
        dini_Set(CONFIG_FILEPATH, "mysql_hostname", "127.0.0.1");
        dini_Set(CONFIG_FILEPATH, "mysql_username", "admin");
        dini_Set(CONFIG_FILEPATH, "mysql_password", "admin");
        dini_Set(CONFIG_FILEPATH, "mysql_database", "red");
        dini_Set(CONFIG_FILEPATH, "run_mode", "2");
    }

    format(pSettings[settings_mysql_hostname], 64, dini_Get(CONFIG_FILEPATH, "mysql_hostname"));
    format(pSettings[settings_mysql_username], 64, dini_Get(CONFIG_FILEPATH, "mysql_username"));
    format(pSettings[settings_mysql_password], 64, dini_Get(CONFIG_FILEPATH, "mysql_password"));
    format(pSettings[settings_mysql_database], 64, dini_Get(CONFIG_FILEPATH, "mysql_database"));
}

stock LoadMySQL()
{
    print("[MySQL] Connecting try...");
    mysql_debug(0);
    mysql_connect("localhost", "root", "test", "");
    new mysql_error1 = mysql_errno();
    if( mysql_error1 == 1044 || mysql_error1 == 1045 || mysql_error1 == 1109 )
    {
        printf("[MySQL] Connecting error [ID:%d]", mysql_error1);
        SendRconCommand("exit");
    }
    else
    {
        print("[MySQL] Connecting success.");
    }
    return 1;
}

stock LoadClass()
{
	for(new i = 0; i < 312; i++)
    {
        AddPlayerClass(i,1707.1323,-1949.5258,14.1172,270.5076,0,0,0,0,0,0);
    }
}

stock LoadConfig()
{
	SetGameModeText("serwer-pomocniczy.pl");
	SendRconCommand("mapname v0.1 | Dxmin");

	ShowPlayerMarkers(0);
    ShowNameTags(0);
    DisableInteriorEnterExits();
    EnableStuntBonusForAll(0);

    for(new Vehicles = 0; Vehicles < MAX_VEHICLES; Vehicles++)
    {
        new string[32];
        new pName[MAX_PLAYER_NAME], playerid;
        new car = GetPlayerName(playerid, pName, sizeof(pName));
        format(string, sizeof(string),"LV - %d",car);
        SetVehicleNumberPlate(Vehicles, string);
    }

    foreachEx(i, MAX_PLAYERS)
    {
        pPlayer[i][char_name_label] = Create3DTextLabel("", 0xFFFFFF99, 0.0, 0.0, 0.0, 20.0, 0, 1);
    }
}