enum ePlayer
{
    char_uid,
    char_name[32],
    char_cash,
    char_pass[28],
    Text3D:char_name_label[128],
    bool:pCanSpawnVehicle,
    pSpawnVehicle,
    char_ac_warns
};
new pPlayer[MAX_PLAYERS][ePlayer];

enum eNoclip
{
    cameramode,
    flyobject,
    mode,
    lrold,
    udold,
    lastmove,
    Float:accelmul
}
new pNoclip[MAX_PLAYERS][eNoclip];

enum eSettings
{
    settings_mysql_hostname[64],
    settings_mysql_username[64],
    settings_mysql_password[64],
    settings_mysql_database[64]
}
new pSettings[eSettings];

enum eItems
{
    item_uid,
    item_name[64],
    item_ownertype,
    item_owner,
    item_type,
    item_value
};
new pItem[MAX_ITEMS][eItems];