//Stock's
stock GetVehicleModelIDFromName(vname[])
{
    for(new i = 0; i < 211; i++)
    {
        if ( strfind(VehicleNames[i], vname, true) != -1 ) return i + 400;
    }
    return -1;
}

stock settime(playerid)
{
    new string[256],year,month,day,hours,minutes,seconds;
    getdate(year, month, day), gettime(hours, minutes, seconds);
    format(string, sizeof string, "%d/%s%d/%s%d", day, ((month < 10) ? ("0") : ("")), month, (year < 10) ? ("0") : (""), year);
    TextDrawSetString(Date, string);
    format(string, sizeof string, "%s%d:%s%d:%s%d", (hours < 10) ? ("0") : (""), hours, (minutes < 10) ? ("0") : (""), minutes, (seconds < 10) ? ("0") : (""), seconds);
    TextDrawSetString(Time, string);
}

stock PlayerName(playerid)
{
    new name[MAX_PLAYER_NAME+1];
    GetPlayerName(playerid, name, sizeof(name));
    return _:name;
}

stock UpdatePlayerStatus(playerid)
{
    if(IsPlayerAdmin(playerid)) return Update3DTextLabelText(pPlayer[playerid][char_name_label], 0xFFFFFFFF, CharNameLabel(playerid));
    Update3DTextLabelText(pPlayer[playerid][char_name_label], 0xFFFFFF99, CharNameLabel(playerid));
    return 1;
}

stock CharNameLabel(playerid)
{
    new front[128];
    if(IsPlayerAdmin(playerid)) format(front, sizeof(front), "{ffffff}%s {cc3b3d}(Administrator, %d)", PlayerName(playerid), playerid);
    else format(front, sizeof(front), "{ffffff}%s {808080}(Gracz, %d)", PlayerName(playerid), playerid);

    return front;
}

stock SetPlayerSkinFix(playerid, skinid)
{
    new
        Float:tmpPos[4],
        vehicleid = GetPlayerVehicleID(playerid),
        seatid = GetPlayerVehicleSeat(playerid);
    GetPlayerPos(playerid, tmpPos[0], tmpPos[1], tmpPos[2]);
    GetPlayerFacingAngle(playerid, tmpPos[3]);
    if(skinid < 0 || skinid > 299) return 0;
    if(GetPlayerSpecialAction(playerid) == SPECIAL_ACTION_DUCK)
    {
        SetPlayerPos(playerid, tmpPos[0], tmpPos[1], tmpPos[2]);
        SetPlayerFacingAngle(playerid, tmpPos[3]);
        TogglePlayerControllable(playerid, 1);
        return SetPlayerSkin(playerid, skinid);
    }
    else if(IsPlayerInAnyVehicle(playerid))
    {
        new
            tmp;
        RemovePlayerFromVehicle(playerid);
        SetPlayerPos(playerid, tmpPos[0], tmpPos[1], tmpPos[2]);
        SetPlayerFacingAngle(playerid, tmpPos[3]);
        TogglePlayerControllable(playerid, 1);
        tmp = SetPlayerSkin(playerid, skinid);
        PutPlayerInVehicle(playerid, vehicleid, (seatid == 128) ? 0 : seatid);
        return tmp;
    }
    else
    {
        return SetPlayerSkin(playerid, skinid);
    }
}

stock GetMoveDirectionFromKeys(ud, lr)
{
    new direction = 0;
    
    if(lr < 0)
    {
        if(ud < 0)      direction = MOVE_FORWARD_LEFT;  // Up & Left key pressed
        else if(ud > 0) direction = MOVE_BACK_LEFT;     // Back & Left key pressed
        else            direction = MOVE_LEFT;          // Left key pressed
    }
    else if(lr > 0)     // Right pressed
    {
        if(ud < 0)      direction = MOVE_FORWARD_RIGHT;  // Up & Right key pressed
        else if(ud > 0) direction = MOVE_BACK_RIGHT;     // Back & Right key pressed
        else            direction = MOVE_RIGHT;          // Right key pressed
    }
    else if(ud < 0)     direction = MOVE_FORWARD;   // Up key pressed
    else if(ud > 0)     direction = MOVE_BACK;      // Down key pressed
    
    return direction;
}

stock GetNextCameraPosition(move_mode, Float:CP[3], Float:FV[3], &Float:X, &Float:Y, &Float:Z)
{
    #define OFFSET_X (FV[0]*6000.0)
    #define OFFSET_Y (FV[1]*6000.0)
    #define OFFSET_Z (FV[2]*6000.0)
    switch(move_mode)
    {
        case MOVE_FORWARD:
        {
            X = CP[0]+OFFSET_X;
            Y = CP[1]+OFFSET_Y;
            Z = CP[2]+OFFSET_Z;
        }
        case MOVE_BACK:
        {
            X = CP[0]-OFFSET_X;
            Y = CP[1]-OFFSET_Y;
            Z = CP[2]-OFFSET_Z;
        }
        case MOVE_LEFT:
        {
            X = CP[0]-OFFSET_Y;
            Y = CP[1]+OFFSET_X;
            Z = CP[2];
        }
        case MOVE_RIGHT:
        {
            X = CP[0]+OFFSET_Y;
            Y = CP[1]-OFFSET_X;
            Z = CP[2];
        }
        case MOVE_BACK_LEFT:
        {
            X = CP[0]+(-OFFSET_X - OFFSET_Y);
            Y = CP[1]+(-OFFSET_Y + OFFSET_X);
            Z = CP[2]-OFFSET_Z;
        }
        case MOVE_BACK_RIGHT:
        {
            X = CP[0]+(-OFFSET_X + OFFSET_Y);
            Y = CP[1]+(-OFFSET_Y - OFFSET_X);
            Z = CP[2]-OFFSET_Z;
        }
        case MOVE_FORWARD_LEFT:
        {
            X = CP[0]+(OFFSET_X  - OFFSET_Y);
            Y = CP[1]+(OFFSET_Y  + OFFSET_X);
            Z = CP[2]+OFFSET_Z;
        }
        case MOVE_FORWARD_RIGHT:
        {
            X = CP[0]+(OFFSET_X  + OFFSET_Y);
            Y = CP[1]+(OFFSET_Y  - OFFSET_X);
            Z = CP[2]+OFFSET_Z;
        }
    }
}

stock GetPlayerSpeed(playerid)
{
    new
        Float:speed_x,
        Float:speed_y,
        Float:speed_z,
        Float:speed_value;

    if(IsPlayerInAnyVehicle(playerid))
    {
        GetVehicleVelocity(GetPlayerVehicleID(playerid), speed_x, speed_y, speed_z);
    }
    else
    {
        GetPlayerVelocity(playerid, speed_x, speed_y, speed_z);
    }

    speed_value = floatsqroot((speed_x * speed_x) + (speed_y * speed_y) + (speed_z * speed_z)) * 200;
    return floatround(speed_value);
}

stock AC_Kick(playerid, Float:speed)
{
    SendClientMessage(playerid, 0xFF0000FF, sprintf("System wykryl korzystanie z niedozwolonych modyfikacji! (v:%.2f)", speed));
    Kick(playerid);
    return 1;
}

//Forward's
forward settime(playerid);

forward GetVehicleModelIDFromName(vname[]);

forward vehicle(playerid);
public vehicle(playerid)
{
    TextDrawHideForPlayer(playerid, VehicleSpawn);
}

forward lataniewlacz(playerid);
public lataniewlacz(playerid)
{
    TextDrawHideForPlayer(playerid, FlyON);
}

forward lataniewylacz(playerid);
public lataniewylacz(playerid)
{
    TextDrawHideForPlayer(playerid, FlyOFF);
}

forward cmd(playerid);
public cmd(playerid)
{
    TextDrawHideForPlayer(playerid, CommandTextDraw);
    return 1;
}

forward radioclose(playerid);
public radioclose(playerid)
{
    TextDrawHideForPlayer(playerid, radiooff);
    return 1;
}

forward radioopen(playerid);
public radioopen(playerid)
{
    TextDrawHideForPlayer(playerid, radioon);
    return 1;
}

forward restart(playerid);
public restart(playerid)
{
    TextDrawHideForAll(RestartTextDraw);
    SendRconCommand("gmx");
    return 1;
}

forward skin(playerid);
public skin(playerid)
{
    TextDrawHideForPlayer(playerid, SkinTextDraw);
    return 1;
}