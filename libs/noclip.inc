//Stock's
stock N_End()
{
    for(new x; x<MAX_PLAYERS; x++)
    {
        if(pNoclip[x][cameramode] == CAMERA_MODE_FLY) CancelFlyMode(x);
    }
}

stock N_Player(playerid)
{
    pNoclip[playerid][cameramode]    = CAMERA_MODE_NONE;
    pNoclip[playerid][lrold]         = 0;
    pNoclip[playerid][udold]         = 0;
    pNoclip[playerid][mode]          = 0;
    pNoclip[playerid][lastmove]      = 0;
    pNoclip[playerid][accelmul]      = 0.0;
}

stock N_PUpdate(playerid)
{
    if(pNoclip[playerid][cameramode] == CAMERA_MODE_FLY)
    {
        new keys,ud,lr;
        GetPlayerKeys(playerid,keys,ud,lr);

        if(pNoclip[playerid][mode] && (GetTickCount() - pNoclip[playerid][lastmove] > 100))
        {
            MoveCamera(playerid);
        }

        if(pNoclip[playerid][udold] != ud || pNoclip[playerid][lrold] != lr)
        {
            if((pNoclip[playerid][udold] != 0 || pNoclip[playerid][lrold] != 0) && ud == 0 && lr == 0)
            {
                StopPlayerObject(playerid, pNoclip[playerid][flyobject]);
                pNoclip[playerid][mode]      = 0;
                pNoclip[playerid][accelmul]  = 0.0;
            }
            else
            {

                pNoclip[playerid][mode] = GetMoveDirectionFromKeys(ud, lr);

                MoveCamera(playerid);
            }
        }
        pNoclip[playerid][udold] = ud; pNoclip[playerid][lrold] = lr;
        return 0;
    }
    return 1;
}

stock MoveCamera(playerid)
{
    new Float:FV[3], Float:CP[3];
    GetPlayerCameraPos(playerid, CP[0], CP[1], CP[2]);  
    GetPlayerCameraFrontVector(playerid, FV[0], FV[1], FV[2]);

    if(pNoclip[playerid][accelmul] <= 1) pNoclip[playerid][accelmul] += ACCEL_RATE;

    new Float:speed = MOVE_SPEED * pNoclip[playerid][accelmul];

    new Float:X, Float:Y, Float:Z;
    GetNextCameraPosition(pNoclip[playerid][mode], CP, FV, X, Y, Z);
    MovePlayerObject(playerid, pNoclip[playerid][flyobject], X, Y, Z, speed);

    pNoclip[playerid][lastmove] = GetTickCount();
    return 1;
}

stock CancelFlyMode(playerid)
{
    DeletePVar(playerid, "FlyMode");
    CancelEdit(playerid);
    TogglePlayerSpectating(playerid, false);

    DestroyPlayerObject(playerid, pNoclip[playerid][flyobject]);
    pNoclip[playerid][cameramode] = CAMERA_MODE_NONE;
    return 1;
}

stock FlyMode(playerid)
{
    new Float:X, Float:Y, Float:Z;
    GetPlayerPos(playerid, X, Y, Z);
    pNoclip[playerid][flyobject] = CreatePlayerObject(playerid, 19300, X, Y, Z, 0.0, 0.0, 0.0);

    TogglePlayerSpectating(playerid, true);
    AttachCameraToPlayerObject(playerid, pNoclip[playerid][flyobject]);

    SetPVarInt(playerid, "FlyMode", 1);
    pNoclip[playerid][cameramode] = CAMERA_MODE_FLY;
    return 1;
}